package mk.test.chucknorrisjokestomche.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.ArrayList;

import mk.test.chucknorrisjokestomche.R;
import mk.test.chucknorrisjokestomche.adapter.JokeAdapter;
import mk.test.chucknorrisjokestomche.interfaces.JokeClickInterface;
import mk.test.chucknorrisjokestomche.model.Joke;
import mk.test.chucknorrisjokestomche.model.JokeResponse;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity implements JokeClickInterface {

    private ArrayList<Joke> jokes = new ArrayList<>();
    private Gson gson;
    private JokeAdapter adapter;
    private RecyclerView recyclerView;
    private OkHttpClient client;
    private String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.recycler_view);

        client = new OkHttpClient();
        url = "https://api.icndb.com/jokes/random/20";

        gson = new Gson();

        recyclerView.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));

        loadJokes();

        adapter = new JokeAdapter(this, jokes, this);
        recyclerView.setAdapter(adapter);
    }

    public void loadJokes(){
        Request request = new Request.Builder().url(url).build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                MainActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(MainActivity.this, "Failed", Toast.LENGTH_SHORT).show();
                    }
                });

            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                if(response.isSuccessful()){
                    String jsonString = response.body().string();
                    JokeResponse jokeResponse = gson.fromJson(jsonString, JokeResponse.class);
                    jokes = jokeResponse.getValue();

                    MainActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            adapter = new JokeAdapter(MainActivity.this, jokes, MainActivity.this);
                            recyclerView.setAdapter(adapter);
                            adapter.notifyDataSetChanged();
                        }
                    });
                }
            }
        });
    }

    @Override
    public void onJokeClick(String joke) {
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Share Chuck Norris joke");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, joke);
        startActivity(Intent.createChooser(sharingIntent, joke));
    }

    public void onRefreshClick(View view) {
        loadJokes();
    }
}
