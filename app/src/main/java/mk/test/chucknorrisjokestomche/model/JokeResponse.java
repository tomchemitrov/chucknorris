package mk.test.chucknorrisjokestomche.model;

import java.util.ArrayList;

public class JokeResponse {

    private String type;
    private ArrayList<Joke> value;

    public JokeResponse() {}

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public ArrayList<Joke> getValue() {
        return value;
    }

    public void setValue(ArrayList<Joke> value) {
        this.value = value;
    }
}
