package mk.test.chucknorrisjokestomche.model;

import java.util.ArrayList;

public class Joke {

    private int id;
    private String joke;
    private ArrayList<String> categories;

    public Joke () {}

    public Joke(String joke, ArrayList<String> categories) {
        this.joke = joke;
        this.categories = categories;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getJoke() {
        return joke;
    }

    public void setJoke(String joke) {
        this.joke = joke;
    }

    public ArrayList<String> getCategories() {
        return categories;
    }

    public void setCategories(ArrayList<String> categories) {
        this.categories = categories;
    }
}
