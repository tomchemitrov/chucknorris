package mk.test.chucknorrisjokestomche.adapter;

import android.content.Context;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import mk.test.chucknorrisjokestomche.R;
import mk.test.chucknorrisjokestomche.interfaces.JokeClickInterface;
import mk.test.chucknorrisjokestomche.model.Joke;

public class JokeAdapter extends RecyclerView.Adapter<JokeAdapter.JokeViewHolder> {

    private Context context;
    private ArrayList<Joke> jokes;
    private LayoutInflater inflater;
    private JokeClickInterface clickInterface;

    public JokeAdapter(Context context, ArrayList<Joke> data, JokeClickInterface clickInterface){
        this.inflater = LayoutInflater.from(context);
        this.context = context;
        this.clickInterface = clickInterface;
        this.jokes = data;
    }


    @NonNull
    @Override
    public JokeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_joke, parent, false);
        return new JokeViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull JokeViewHolder holder, int position) {
        Joke joke = jokes.get(position);

        holder.jokeNumber.setText(context.getString(R.string.joke_number) + (position + 1));
        holder.joke.setText(joke.getJoke());

        if(joke.getCategories().size() != 0){
            holder.jokeCategory.setText(joke.getCategories().get(0));
       } else {
            holder.jokeCategory.setText(context.getResources().getString(R.string.undefined));
        }
    }

    @Override
    public int getItemCount() {
        return jokes.size();
    }

    public class JokeViewHolder extends RecyclerView.ViewHolder {

        private TextView jokeNumber;
        private TextView joke;
        private TextView jokeCategory;

        public JokeViewHolder(@NonNull View itemView) {
            super(itemView);

            jokeNumber = itemView.findViewById(R.id.joke_number);
            joke = itemView.findViewById(R.id.joke);
            jokeCategory = itemView.findViewById(R.id.joke_category);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    clickInterface.onJokeClick(jokes.get(getAdapterPosition()).getJoke());
                }
            });
        }
    }
}
